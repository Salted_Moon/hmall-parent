package com.hmall.api.config;

import com.hmall.common.constants.UserConstant;
import com.hmall.common.utils.UserContext;
import feign.Logger;
import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;


public class DefaultFeignConfig {
    @Bean
    public Logger.Level feignLogLevel() {
        return Logger.Level.FULL;
    }

    //微服务之间传递用户信息的拦截器
    @Bean
    public RequestInterceptor userInfoRequestInterceptor() {
        return requestTemplate -> {
            Long userId = UserContext.getUser();
            if (userId == null) {
                return;
            }
            requestTemplate.header(UserConstant.USER_INFO, userId.toString());
        };
    }
}
