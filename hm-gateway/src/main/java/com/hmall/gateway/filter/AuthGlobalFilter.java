package com.hmall.gateway.filter;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.text.AntPathMatcher;
import com.hmall.common.constants.UserConstant;
import com.hmall.gateway.config.AuthProperties;
import com.hmall.gateway.utils.JwtTool;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description: 登录校验
 * @Version: V1.0
 */
@Component
@Slf4j
public class AuthGlobalFilter implements GlobalFilter, Ordered {
    @Resource
    private AuthProperties authProperties;

    @Resource
    private JwtTool jwtTool;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //1 获取请求路径
        ServerHttpRequest request = exchange.getRequest();
        String reqPath = request.getPath().toString();

        //2 无需拦截验证直接放行
        if (isExclude(reqPath)) {
            log.info("reqPath Release success, reqPath:{}",reqPath);
            return chain.filter(exchange);
        }

        //3 请求头获取 jwtToken
        List<String> jwtTokens = request.getHeaders().get("authorization");
        if (CollUtil.isEmpty(jwtTokens)) {
            log.debug("jwtTokens is null");
            ServerHttpResponse response = exchange.getResponse();
            response.setRawStatusCode(401);
            return response.setComplete();
        }
        //4 解析token
        Long userId = null;
        try {
            userId = jwtTool.parseToken(jwtTokens.get(0));
        } catch (Exception e) {
            //4.1 验证失败,直接拒绝访问并返回 401状态码
            log.error("parseToken error:{}", jwtTokens.get(0));
            ServerHttpResponse response = exchange.getResponse();
            response.setRawStatusCode(401);
            return response.setComplete();
        }

        //5 如果解析token成功,传递给下游微服务
        String userIdStr = userId.toString();
        exchange.mutate().request(build ->
                build.header(UserConstant.USER_INFO, userIdStr)).build();

        //6 放行
        return chain.filter(exchange);
    }

    /**
     * 判断是否允许放行
     * @param reqPath
     * @return
     */
    private boolean isExclude(String reqPath) {
        AntPathMatcher antPathMatcher = new AntPathMatcher();
        List<String> excludePaths = authProperties.getExcludePaths();
        for (String excludePath : excludePaths) {
            if (antPathMatcher.match(excludePath, reqPath)) {
                return true;
            }
        }
        return false;
    }
    @Override
    public int getOrder() {
        return -1;
    }
}
